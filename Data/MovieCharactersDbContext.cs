﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Model.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Data
{
    public class MovieCharactersDbContext : DbContext
    {
    public DbSet<Character> Characters { get; set; }
    public  DbSet<Franchise> Franchises { get; set; }
    public DbSet<Movie> Movies { get; set; }

    public MovieCharactersDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {

        }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.EnableSensitiveDataLogging(true);
            optionsBuilder.UseSqlServer(@"Data Source = localhost; initial Catalog = MovieDb; Integrated Security = true");
        }
        /// <summary>
        /// Seeding method for each table uncommented as the migration has been already performed        /// 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Character>().HasData(
                new Character() { Id = 1, Fullname = @"Bruce Willis", Alias = @"", Gender = @"Male", Picture = @"https://www.imdb.com/name/nm0000246/mediaviewer/rm2221768704/" },
                new Character() { Id = 2, Fullname = @"Milla Jovovich", Alias = @"", Gender = @"Female", Picture = @"https://www.imdb.com/name/nm0000170/mediaviewer/rm298158080/" },
                new Character() { Id = 3, Fullname = @"Edward Norton", Alias = @"", Gender = @"Male", Picture = @"https://www.imdb.com/name/nm0001570/mediaviewer/rm3529540352/" },
                new Character() { Id = 4, Fullname = @"Tom Hanks", Alias = @"", Gender = @"Male", Picture = @"https://www.imdb.com/name/nm0000158/mediaviewer/rm3040001536/" });

            modelBuilder.Entity<Franchise>().HasData(
                new Franchise() { Id = 1, Name = @"Sony Pictures (United States)", Description = @"Sony Pictures Entertainment Inc. is an American diversified multinational mass media and entertainment studio conglomerate that produces, acquires, and distributes filmed entertainment through multiple platforms." },
                new Franchise() { Id = 2, Name = @"Fox 2000 Pictures", Description = @"Fox 2000 Pictures was an American film production company within The Walt Disney Studios. " },
                new Franchise() { Id = 3, Name = @"Paramount Pictures", Description = @"Paramount Pictures Corporation is an American film and television production and distribution company and the main namesake subsidiary of Paramount Global. " },
                new Franchise() { Id = 4, Name = @"Warner Bros. Entertainment Inc.", Description = @"Warner Bros. Entertainment Inc. is an American diversified multinational mass media and entertainment conglomerate headquartered at the Warner Bros." });

            modelBuilder.Entity<Movie>().HasData(
                new Movie() { Id = 1, Movietitle = @"The Fifth Element", Genre = @"Sci-Fi", Releaseyear = 1997, Director = @"Nicolas Coquard", Picture = @"https://www.imdb.com/title/tt0377917/mediaviewer/rm1882358784/", Trailer = @"https://www.imdb.com/video/vi2578758681?ref_=tt_pv_vi_aiv_1" },
                new Movie() { Id = 2, Movietitle = @"Resident Evil", Genre = @"Horror", Releaseyear = 2002, Director = @"Paul W.S. Anderson", Picture = @"https://www.imdb.com/title/tt0120804/mediaviewer/rm1380161537/", Trailer = @"https://www.imdb.com/video/vi2894266649?ref_=tt_pv_vi_aiv_1" },
                new Movie() { Id = 3, Movietitle = @"Fight Club", Genre = @"Drama", Releaseyear = 1999, Director = @"David Fincher", Picture = @"https://www.imdb.com/title/tt0137523/mediaviewer/rm1412004864/", Trailer = @"https://www.imdb.com/video/vi781228825/?ref_=tt_vi_i_1" },
                new Movie() { Id = 4, Movietitle = @"Forrest Gump", Genre = @"Drama", Releaseyear = 1994, Director = @"Robert Zemeckis", Picture = @"https://www.imdb.com/title/tt0109830/mediaviewer/rm1954748672/", Trailer = @"https://www.imdb.com/video/vi1748679193/?ref_=tt_vi_i_1" });
                
        }
    }
}
