# MovieCharactersAPI

Our c# ASP.Net application.


## Getting Started

- Clone to a local directory.
- Open solution in Visual Studio
- Change the appsettigns.json to point to your SQL-Server 
- Run the following command to create the database and to seed the database using our base values:

```
update-database
```
 
- Run the application

### Prerequisites

.NET Framework
MS-SQL-Server
Visual Studio 2019/22

### Usage of our API application

Run the application use swagger to try out our API endpoints



## Authors

***Robert Benker** [Robert.Benker@de.experis.com]
***Matthias Friedrich** [Matthias.Friedrich@de.experis.com]
***Thomas Bauer** [Thomas.Bauer@de.experis.com]