﻿using AutoMapper;
using MovieCharactersAPI.Model.Domain;
using MovieCharactersAPI.Model.DTOs.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>().ReverseMap();
            CreateMap<Character, CharacterUpdateDTO>().ReverseMap();
        }
    }
}
