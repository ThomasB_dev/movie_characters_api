﻿using AutoMapper;
using MovieCharactersAPI.Model.Domain;
using MovieCharactersAPI.Model.DTOs.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {

            CreateMap<Franchise, FranchiseReadDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseUpdateDTO>().ReverseMap();
        }
    }
}
