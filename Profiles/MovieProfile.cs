﻿using AutoMapper;
using MovieCharactersAPI.Model.Domain;
using MovieCharactersAPI.Model.DTOs.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>().ReverseMap();
            CreateMap<Movie, MovieUpdateDTO>().ReverseMap();
        }
    }
}
