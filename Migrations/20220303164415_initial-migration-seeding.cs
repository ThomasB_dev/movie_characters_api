﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharactersAPI.Migrations
{
    public partial class initialmigrationseeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Fullname = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Movietitle = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Genre = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Releaseyear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    CharactersId = table.Column<int>(type: "int", nullable: false),
                    MoviesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.CharactersId, x.MoviesId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Characters_CharactersId",
                        column: x => x.CharactersId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movies_MoviesId",
                        column: x => x.MoviesId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    MovieId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Franchises_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "Fullname", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "", "Bruce Willis", "Male", "https://www.imdb.com/name/nm0000246/mediaviewer/rm2221768704/" },
                    { 2, "", "Milla Jovovich", "Female", "https://www.imdb.com/name/nm0000170/mediaviewer/rm298158080/" },
                    { 3, "", "Edward Norton", "Male", "https://www.imdb.com/name/nm0001570/mediaviewer/rm3529540352/" },
                    { 4, "", "Tom Hanks", "Male", "https://www.imdb.com/name/nm0000158/mediaviewer/rm3040001536/" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "MovieId", "Name" },
                values: new object[,]
                {
                    { 1, "Sony Pictures Entertainment Inc. is an American diversified multinational mass media and entertainment studio conglomerate that produces, acquires, and distributes filmed entertainment through multiple platforms.", null, "Sony Pictures (United States)" },
                    { 2, "Fox 2000 Pictures was an American film production company within The Walt Disney Studios. ", null, "Fox 2000 Pictures" },
                    { 3, "Paramount Pictures Corporation is an American film and television production and distribution company and the main namesake subsidiary of Paramount Global. ", null, "Paramount Pictures" },
                    { 4, "Warner Bros. Entertainment Inc. is an American diversified multinational mass media and entertainment conglomerate headquartered at the Warner Bros.", null, "Warner Bros. Entertainment Inc." }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "Genre", "Movietitle", "Picture", "Releaseyear", "Trailer" },
                values: new object[,]
                {
                    { 1, "Nicolas Coquard", "Sci-Fi", "The Fifth Element", "https://www.imdb.com/title/tt0377917/mediaviewer/rm1882358784/", 1997, "https://www.imdb.com/video/vi2578758681?ref_=tt_pv_vi_aiv_1" },
                    { 2, "Paul W.S. Anderson", "Horror", "Resident Evil", "https://www.imdb.com/title/tt0120804/mediaviewer/rm1380161537/", 2002, "https://www.imdb.com/video/vi2894266649?ref_=tt_pv_vi_aiv_1" },
                    { 3, "David Fincher", "Drama", "Fight Club", "https://www.imdb.com/title/tt0137523/mediaviewer/rm1412004864/", 1999, "https://www.imdb.com/video/vi781228825/?ref_=tt_vi_i_1" },
                    { 4, "Robert Zemeckis", "Drama", "Forrest Gump", "https://www.imdb.com/title/tt0109830/mediaviewer/rm1954748672/", 1994, "https://www.imdb.com/video/vi1748679193/?ref_=tt_vi_i_1" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_MoviesId",
                table: "CharacterMovie",
                column: "MoviesId");

            migrationBuilder.CreateIndex(
                name: "IX_Franchises_MovieId",
                table: "Franchises",
                column: "MovieId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Franchises");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");
        }
    }
}
