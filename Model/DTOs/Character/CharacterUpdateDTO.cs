﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Model.DTOs.Character
{
    public class CharacterUpdateDTO
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Fullname { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        [MaxLength(1000)]
        public string Picture { get; set; }
    }
}
