﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Model.DTOs.Movie
{
    public class MovieReadDTO
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Movietitle { get; set; }
        [MaxLength(50)]
        public string Genre { get; set; }

        public int Releaseyear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }

        [MaxLength(1000)]
        public string Picture { get; set; }
        [MaxLength(1000)]
        public string Trailer { get; set; }
    }
}
