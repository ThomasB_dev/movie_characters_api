﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Model.Domain;
using MovieCharactersAPI.Model.DTOs.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Controllers
{
    /// <summary>
    /// Character Controller Class
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]

    public class CharacterController : ControllerBase
    {
        private MovieCharactersDbContext _context;
        private IMapper _mapper;

        /// <summary>
        /// Character Controller method
        /// </summary>
        public CharacterController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Receive all characters 
        /// </summary>
        /// <remarks>
        /// You will receive all available characters from our database once you execute.
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public ActionResult<IEnumerable <Character>> GetCharacters()
        {   
            var charList = _context.Characters.ToList<Character>();
            var charToProvide = _mapper.Map<List<CharacterReadDTO>>(charList);
            

            return Ok(charToProvide);
        }

        /// <summary>
        /// Updating character in movies
        /// </summary>
        /// <remarks>
        /// Here you can assign one specific character to one or more movies
        /// </remarks>
        /// <returns></returns>
        [HttpPost("assignchartomovie/{id}")]
        public async Task<ActionResult> AssignCharacterToMovie(int id, [FromBody] List<int> movies)
        {
            var chars = await _context.Characters.Include(c => c.Movies).FirstOrDefaultAsync(c => c.Id == id);

            if (chars == null)
            {
                return NotFound();
            }

            foreach (var movieId in movies)
            {
                var tempMovie = await _context.Movies.FirstOrDefaultAsync(m => m.Id == movieId);
                if (tempMovie != null)
                {
                    chars.Movies.Add(tempMovie);
                }

            }

            await _context.SaveChangesAsync();
            return NoContent();
        }



    }
}
