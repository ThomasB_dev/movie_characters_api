﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Model.Domain;
using MovieCharactersAPI.Model.DTOs.Character;
using MovieCharactersAPI.Model.DTOs.Franchise;
using MovieCharactersAPI.Model.DTOs.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchiseController : ControllerBase
    {
        private MovieCharactersDbContext _context;
        private IMapper _mapper;


        public FranchiseController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all movies in a franchise 
        /// </summary>
        /// <remarks>
        /// You will receive all available movies assigned to a specific franchise from our database once you execute.
        /// </remarks>
        /// <returns></returns>        
        [HttpGet("movie/{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<MovieReadDTO>>> GetMoviesFromFranchises(int id)
        {

            var movieList = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);


            if (movieList == null)
            {
                return NotFound();
            }


            return _mapper.Map<List<MovieReadDTO>>(movieList.Movies.ToList());
        }


        /// <summary>
        /// Update a franchise by id
        /// </summary>
        /// <remarks>
        /// Update a specific franchise by id in our database once you execute.
        /// </remarks>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        public ActionResult UpdateFranchise(int id, Franchise newFranchise)
        {

            if (id != newFranchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(newFranchise).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        /// <summary>
        /// Get all the characters in a franchise 
        /// </summary>
        /// <remarks>
        /// Specify a franchsie and you will receive all the characters
        /// </remarks>
        /// <returns></returns>

        [HttpGet("charinfranchise/{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetCharactersByFranchiseId(int id)
        {

            var movieList = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);

            var charList = await _context.Movies.Include(c => c.Characters).FirstOrDefaultAsync(c => c.Id == movieList.Id);

            if (movieList == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<CharacterReadDTO>>(charList.Characters.ToList());            
        }
    }
}
