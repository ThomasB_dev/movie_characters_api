﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Model.Domain;
using MovieCharactersAPI.Model.DTOs.Character;
using MovieCharactersAPI.Model.DTOs.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private MovieCharactersDbContext _context;
        private IMapper _mapper;

        public MovieController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all available movies
        /// </summary>
        /// <remarks>
        /// Here you will receive all available movies
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public ActionResult<IEnumerable<Movie>> GetMovies()
        {

            var movieList = _context.Movies.ToList<Movie>();

            return Ok(movieList);
        }
        /// <summary>
        /// Update a franchise to one movie
        /// </summary>
        /// <remarks>
        /// Here you can assign one specific franchise to one specific movie
        /// </remarks>
        /// <returns></returns>
        [HttpPost("{id}")]
        public async Task<ActionResult> AssignFranchiseToMovie(int id, [FromBody] int franchiseId)
        {

            var franchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);
            var movie = await _context.Movies.FindAsync(franchiseId);

            if (franchise == null)
            {
                return NotFound();
            }

            franchise.Movies.Add(movie);

            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Get all the characters in a movie 
        /// </summary>
        /// <remarks>
        /// Specify a movie and you will receive all the characters
        /// </remarks>
        /// <returns></returns>

        [HttpGet("charinmovie/{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetCharactersByMovieId(int id)
        {

            var movieList = await _context.Movies.Include(c => c.Characters).FirstOrDefaultAsync(c => c.Id == id);


            if (movieList == null)
            {
                return NotFound();
            }


            return _mapper.Map<List<CharacterReadDTO>>(movieList.Characters.ToList());
        }




    }  

}
